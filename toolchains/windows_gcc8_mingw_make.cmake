
set(CMAKE_C_COMPILER gcc)
set(CMAKE_CXX_COMPILER g++)
# This not works. See:
# - https://gitlab.com/scandyna/mdt-cmake-modules/-/jobs/8456121726
# - https://stackoverflow.com/questions/11269833/cmake-selecting-a-generator-within-cmakelists-txt
# set(CMAKE_GENERATOR "MinGW Makefiles")
